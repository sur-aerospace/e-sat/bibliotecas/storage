#include "w25q.h"

void W25Q_GetDeviceID(W25Q_HandleTypeDef *hstorage, uint8_t *info)
{
  W25Q_CommandHandler cmd = hstorage->HAL.Command;
  uint8_t dummy[3] = {0x00, 0x00, 0x00};
  cmd(CMD_DEVICE_ID, dummy, 3, info, 2);
}

void W25Q_GetJedecID(W25Q_HandleTypeDef *hstorage, uint8_t *info)
{
  W25Q_CommandHandler cmd = hstorage->HAL.Command;
  cmd(CMD_JEDEC_ID, NULL, 0, info, 3);
}

//*************************************************************************************//

void W25Q_WriteEnable(W25Q_HandleTypeDef *hstorage)
{
  W25Q_CommandHandler cmd = hstorage->HAL.Command;
  cmd(CMD_WRITE_ENABLE, NULL, 0, NULL, 0);
}

void W25Q_WriteDisable(W25Q_HandleTypeDef *hstorage)
{
  W25Q_CommandHandler cmd = hstorage->HAL.Command;
  cmd(CMD_WRITE_DISABLE, NULL, 0, NULL, 0);
}

void W25Q_GetStatusReg1(W25Q_HandleTypeDef *hstorage, uint8_t *info)
{
  W25Q_CommandHandler cmd = hstorage->HAL.Command;
  cmd(CMD_READ_STATUS_REG1, NULL, 0, info, 1);
}

void W25Q_GetStatusReg2(W25Q_HandleTypeDef *hstorage, uint8_t *info)
{
  W25Q_CommandHandler cmd = hstorage->HAL.Command;
  cmd(CMD_READ_STATUS_REG2, NULL, 0, info, 1);
}

void W25Q_GetStatusReg3(W25Q_HandleTypeDef *hstorage, uint8_t *info)
{
  W25Q_CommandHandler cmd = hstorage->HAL.Command;
  cmd(CMD_READ_STATUS_REG3, NULL, 0, info, 1);
}

void W25Q_ReadPage(W25Q_HandleTypeDef *hstorage, uint32_t address, uint8_t *buffer, size_t size)
{
  W25Q_CommandHandler cmd = hstorage->HAL.Command;
  uint8_t dummy[4];
  dummy[0] = (address >> 16) & 0xFF;
  dummy[1] = (address >> 8) & 0xFF;
  dummy[2] = (address >> 0) & 0xFF;
  dummy[3] = 0x00;
  cmd(CMD_FAST_READ, dummy, 4, buffer, size);
}

void W25Q_WritePage(W25Q_HandleTypeDef *hstorage, uint32_t address, uint8_t *buffer, size_t size)
{
  W25Q_CommandHandler cmd = hstorage->HAL.Command;
  uint8_t dummy[3];
  dummy[0] = (address >> 16) & 0xFF;
  dummy[1] = (address >> 8) & 0xFF;
  dummy[2] = (address >> 0) & 0xFF;
  cmd(CMD_PAGE_PROGRAM, dummy, 3, buffer, size);
}

void W25Q_ChipErase(W25Q_HandleTypeDef *hstorage)
{
  W25Q_CommandHandler cmd = hstorage->HAL.Command;
  cmd(CMD_CHIP_ERASE, NULL, 0, NULL, 0);
}