#ifndef __W25Q_TYPES_H
#define __W25Q_TYPES_H

#include <stddef.h>
#include <stdint.h>

#ifdef __cplusplus
extern "C" {
#endif /* __cplusplus */

typedef void (*W25Q_CommandHandler)(uint8_t Command, uint8_t *Control, size_t CtrlSize, uint8_t *Buffer, size_t BuffSize);

typedef struct
{
  W25Q_CommandHandler Command;
} W25Q_HalTypeDef;

typedef struct
{
  W25Q_HalTypeDef HAL;
} W25Q_HandleTypeDef;

#ifdef __cplusplus
}
#endif /* __cplusplus */
#endif /* __W25Q_TYPES_H */