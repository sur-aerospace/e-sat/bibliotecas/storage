#ifndef __W25Q_H
#define __W25Q_H

#include "w25q_api.h"
#include "w25q_defs.h"

#ifdef __cplusplus
extern "C" {
#endif /* __cplusplus */

void W25Q_GetDeviceID(W25Q_HandleTypeDef *hstorage, uint8_t *info);
void W25Q_GetJedecID(W25Q_HandleTypeDef *hstorage, uint8_t *info);

//*************************************************************************************//

void W25Q_WriteEnable(W25Q_HandleTypeDef *hstorage);
void W25Q_WriteDisable(W25Q_HandleTypeDef *hstorage);
void W25Q_GetStatusReg1(W25Q_HandleTypeDef *hstorage, uint8_t *info);
void W25Q_GetStatusReg2(W25Q_HandleTypeDef *hstorage, uint8_t *info);
void W25Q_GetStatusReg3(W25Q_HandleTypeDef *hstorage, uint8_t *info);
void W25Q_ReadPage(W25Q_HandleTypeDef *hstorage, uint32_t address, uint8_t *buffer, size_t size);
void W25Q_WritePage(W25Q_HandleTypeDef *hstorage, uint32_t address, uint8_t *buffer, size_t size);
void W25Q_ChipErase(W25Q_HandleTypeDef *hstorage);

#ifdef __cplusplus
}
#endif /* __cplusplus */
#endif /* __W25Q_H */