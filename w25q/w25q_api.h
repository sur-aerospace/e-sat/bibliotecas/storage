// W25Q commands (from API documentation)
#define CMD_WRITE_ENABLE           0x06
#define CMD_WRITE_DISABLE          0x04
#define CMD_READ_STATUS_REG1       0x05
#define CMD_WRITE_STATUS_REG1      0x01
#define CMD_READ_STATUS_REG2       0x35
#define CMD_WRITE_STATUS_REG2      0x31
#define CMD_READ_STATUS_REG3       0x15
#define CMD_WRITE_STATUS_REG3      0x11
#define CMD_CHIP_ERASE             0xC7
#define CMD_PROGRAM_SUSPEND        0x75
#define CMD_PROGRAM_RESUME         0x7A
#define CMD_POWER_DOWN             0xB9
#define CMD_DEVICE_ID              0x90
#define CMD_JEDEC_ID               0x9F
#define CMD_ENABLE_RESET           0x66
#define CMD_RESET_DEVICE           0x99
#define CMD_READ_UNIQUE_ID         0x4B
#define CMD_PAGE_PROGRAM           0x02
#define CMD_QUAD_PAGE_PROGRAM      0x32
#define CMD_SECTOR_ERASE_4K        0x20
#define CMD_SECTOR_ERASE_32K       0x52
#define CMD_SECTOR_ERASE_64K       0xD8
#define CMD_READ_DATA              0x03
#define CMD_FAST_READ              0x0B





